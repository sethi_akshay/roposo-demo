package com.roposo.akshay.storydemo.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;

import com.roposo.akshay.storydemo.R;
import com.roposo.akshay.storydemo.fragments.StoryDetailFragment;
import com.roposo.akshay.storydemo.modals.Story;

/**
 * Created by AkshaySethi on 09/04/16.
 */
public class StoryDetailActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_story_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.detail_toolbar);
        if (toolbar != null) {
            toolbar.setBackgroundColor(Color.TRANSPARENT);
            toolbar.setTitle("");
            setSupportActionBar(toolbar);
        }

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        if (savedInstanceState == null) {

            Bundle arguments = new Bundle();
            arguments.putSerializable(StoryDetailFragment.ARG_ITEM_ID,
                    getIntent().getSerializableExtra(StoryDetailFragment.ARG_ITEM_ID));
            StoryDetailFragment fragment = new StoryDetailFragment();
            fragment.setArguments(arguments);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.card_view, fragment)
                    .commit();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {

            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {

        if (((StoryDetailFragment) getSupportFragmentManager().getFragments().get(0)).isFollowingStatusChanged()) {
            Intent returnIntent = new Intent();
            returnIntent.putExtra("author", ((Story) getIntent().getSerializableExtra(StoryDetailFragment.ARG_ITEM_ID)).getAuthor());
            returnIntent.putExtra("isFollowing", ((StoryDetailFragment) getSupportFragmentManager().getFragments().get(0)).getNewIsFollowingStatus());
            setResult(Activity.RESULT_OK, returnIntent);
            finish();
        } else {
            super.onBackPressed();
        }
    }
}
