package com.roposo.akshay.storydemo.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.roposo.akshay.storydemo.R;
import com.roposo.akshay.storydemo.activities.StoryDetailActivity;
import com.roposo.akshay.storydemo.activities.StoryListActivity;
import com.roposo.akshay.storydemo.fragments.StoryDetailFragment;
import com.roposo.akshay.storydemo.modals.Author;
import com.roposo.akshay.storydemo.modals.Story;

import java.util.List;

/**
 * Created by AkshaySethi on 09/04/16.
 */
public class StoryListRecyclerViewAdapter
        extends RecyclerView.Adapter<StoryListRecyclerViewAdapter.ViewHolder> {

    private List<Story> mStoryList;
    private Context mContext;
    private OnFollowButtonClickListener onFollowButtonClickListener;

    public interface OnFollowButtonClickListener{
        void onFollowButtonClick(Author author, boolean isFollowing);
    }

    public StoryListRecyclerViewAdapter(Context context, List<Story> storyList, OnFollowButtonClickListener onFollowButtonClickListener) {
        mContext = context;
        mStoryList = storyList;
        this.onFollowButtonClickListener = onFollowButtonClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.story_list_content, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mStory = mStoryList.get(position);
        holder.tvStoryTitle.setText(holder.mStory.getTitle());
        holder.tvStoryDescription.setText(holder.mStory.getDescription());

        if (holder.mStory.getAuthor() != null) {
            holder.tvAuthorName.setText(holder.mStory.getAuthor().getUsername());
            holder.tvAuthorHandle.setText(holder.mStory.getAuthor().getHandle());
            Glide.with(mContext).load(holder.mStory.getAuthor().getImage()).into(holder.ivAuthorImage);
        } else {
            holder.tvAuthorName.setVisibility(View.GONE);
            holder.tvAuthorHandle.setVisibility(View.GONE);
        }

        if (holder.mStory.getAuthor().getIsFollowing()) {
            holder.btnFollow.setText("Following");
            holder.btnFollow.setBackgroundResource(R.drawable.btn_following);
            holder.btnFollow.setTextColor(Color.WHITE);
        } else {
            holder.btnFollow.setText("Follow");
            holder.btnFollow.setBackgroundResource(R.drawable.btn_follow);
            holder.btnFollow.setTextColor(Color.BLACK);
        }

        holder.btnFollow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.mStory.getAuthor().getIsFollowing()) {

                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext)
                            .setMessage("Are you sure you want to Unfollow " + holder.mStory.getAuthor().getUsername() + "?")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    holder.mStory.getAuthor().setIsFollowing(false);
                                    holder.btnFollow.setText("Follow");
                                    holder.btnFollow.setBackgroundResource(R.drawable.btn_follow);
                                    holder.btnFollow.setTextColor(Color.BLACK);
                                    onFollowButtonClickListener.onFollowButtonClick(holder.mStory.getAuthor(), false);
                                }
                            })
                            .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();

                } else {
                    holder.mStory.getAuthor().setIsFollowing(true);
                    holder.btnFollow.setText("Following");
                    holder.btnFollow.setBackgroundResource(R.drawable.btn_following);
                    holder.btnFollow.setTextColor(Color.WHITE);
                    onFollowButtonClickListener.onFollowButtonClick(holder.mStory.getAuthor(), true);
                }
            }
        });


        Glide.with(mContext).load(holder.mStory.getSi()).diskCacheStrategy(DiskCacheStrategy.ALL).into(holder.ivStoryImage);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(mContext, StoryDetailActivity.class);
                intent.putExtra(StoryDetailFragment.ARG_ITEM_ID, holder.mStory);

                ((StoryListActivity) mContext).startActivityForResult(intent, 1);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mStoryList.size();
    }

    @Override
    public void onViewRecycled(ViewHolder holder) {
        Glide.clear(holder.ivStoryImage);
        holder.btnFollow.setText("");
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView tvStoryTitle, tvStoryDescription, tvAuthorName, tvAuthorHandle;
        public final ImageView ivAuthorImage, ivStoryImage;
        public final Button btnFollow;
        public Story mStory;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            tvStoryTitle = (TextView) view.findViewById(R.id.tvStoryTitle);
            tvStoryDescription = (TextView) view.findViewById(R.id.tvStoryDescription);
            tvAuthorName = (TextView) view.findViewById(R.id.tvAuthorName);
            tvAuthorHandle = (TextView) view.findViewById(R.id.tvAuthorHandle);
            ivAuthorImage = (ImageView) view.findViewById(R.id.ivAuthorImage);
            ivStoryImage = (ImageView) view.findViewById(R.id.ivStoryImage);
            btnFollow = (Button) view.findViewById(R.id.btnFollow);
        }
    }


    /* public void followAllStoriesFromThisAuthor(Author author, boolean isFollowing) {
        for (int i = 0; i < mStoryList.size(); i++) {
            Story story = mStoryList.get(i);

            if (story.getAuthor() == null && author == null) {
                story.getAuthor().setIsFollowing(isFollowing);
            } else if (story.getAuthor() != null && author != null && story.getAuthor().getUsername().equals(author.getUsername())) {
                story.getAuthor().setIsFollowing(isFollowing);
            }
        }
    }*/
}
