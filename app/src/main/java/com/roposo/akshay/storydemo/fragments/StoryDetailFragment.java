package com.roposo.akshay.storydemo.fragments;

import android.app.Activity;
import android.content.DialogInterface;
import android.graphics.Color;
import android.support.design.widget.CollapsingToolbarLayout;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.roposo.akshay.storydemo.R;
import com.roposo.akshay.storydemo.modals.Story;


/**
 * Created by AkshaySethi on 09/04/16.
 */
public class StoryDetailFragment extends Fragment {

    public static final String ARG_ITEM_ID = "item";

    private ImageView ivStoryImage, ivAuthorImage;
    private TextView tvAuthorName, tvAuthorHandle;
    private Button btnFollow;

    private Story story;

    private boolean isFollowingStatusChanged = false, newIsFollowingStatus = false;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public StoryDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey(ARG_ITEM_ID)) {
            story = (Story) getArguments().getSerializable(ARG_ITEM_ID);

            final Activity activity = this.getActivity();
            CollapsingToolbarLayout appBarLayout = (CollapsingToolbarLayout) activity.findViewById(R.id.toolbar_layout);
            ivStoryImage = (ImageView) activity.findViewById(R.id.ivStoryImage);
            ivAuthorImage = (ImageView) activity.findViewById(R.id.ivAuthorImage);

            tvAuthorName = (TextView) activity.findViewById(R.id.tvAuthorName);
            tvAuthorHandle = (TextView) activity.findViewById(R.id.tvAuthorHandle);

            btnFollow = (Button) activity.findViewById(R.id.btnFollow);

            if (appBarLayout != null) {
                Glide.with(activity).load(story.getSi()).diskCacheStrategy(DiskCacheStrategy.ALL).into(ivStoryImage);

                tvAuthorName.setText(story.getAuthor().getUsername());
                tvAuthorHandle.setText(story.getAuthor().getHandle());
                Glide.with(activity).load(story.getAuthor().getImage()).into(ivAuthorImage);

                if (story.getAuthor().getIsFollowing()) {
                    btnFollow.setText("Following");
                    btnFollow.setBackgroundResource(R.drawable.btn_following);
                    btnFollow.setTextColor(Color.WHITE);
                } else {
                    btnFollow.setText("Follow");
                    btnFollow.setBackgroundResource(R.drawable.btn_follow);
                    btnFollow.setTextColor(Color.BLACK);
                }


                btnFollow.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (story.getAuthor().getIsFollowing()) {

                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(activity)
                                    .setMessage("Are you sure you want to Unfollow " + story.getAuthor().getUsername() + "?")
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            isFollowingStatusChanged = true;
                                            newIsFollowingStatus = false;
                                            btnFollow.setText("Follow");
                                            btnFollow.setBackgroundResource(R.drawable.btn_follow);
                                            btnFollow.setTextColor(Color.BLACK);
                                            story.getAuthor().setIsFollowing(false);
                                        }
                                    })
                                    .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    });
                            alertDialog.show();

                        } else {
                            isFollowingStatusChanged = true;
                            newIsFollowingStatus = true;
                            story.getAuthor().setIsFollowing(true);

                            btnFollow.setText("Following");
                            btnFollow.setBackgroundResource(R.drawable.btn_following);
                            btnFollow.setTextColor(Color.WHITE);
                        }
                    }
                });
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.story_detail, container, false);

        if (story != null) {
            ((TextView) rootView.findViewById(R.id.tvStoryTitle)).setText(story.getTitle());
            ((TextView) rootView.findViewById(R.id.tvStoryDescription)).setText(story.getDescription());
            ((TextView) rootView.findViewById(R.id.tvStoryLikes)).setText(story.getLikesCount() + " Likes");
            ((TextView) rootView.findViewById(R.id.tvStoryComments)).setText(story.getCommentCount() + " Comments");
        }

        return rootView;
    }


    public boolean isFollowingStatusChanged() {
        return isFollowingStatusChanged;
    }

    public boolean getNewIsFollowingStatus(){
        return newIsFollowingStatus;
    }
}
