package com.roposo.akshay.storydemo.utils;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.roposo.akshay.storydemo.modals.Author;
import com.roposo.akshay.storydemo.modals.Content;
import com.roposo.akshay.storydemo.modals.Story;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Created by AkshaySethi on 09/04/16.
 */
public class ContentDeserializer implements JsonDeserializer<Content> {
    public Content deserialize(JsonElement json, Type typeOfT,
                               JsonDeserializationContext context) {

        Content content = new Content();
        ArrayList<Author> authorList = new ArrayList<>();
        ArrayList<Story> storyList = new ArrayList<>();

        JsonArray array = json.getAsJsonArray();
        for (int i = 0; i < array.size(); i++) {
            if (array.get(i).getAsJsonObject().has("username")) {
                Author author = context.deserialize(array.get(i).getAsJsonObject(), Author.class);
                authorList.add(author);
            } else {
                Story story = context.deserialize(array.get(i).getAsJsonObject(), Story.class);
                storyList.add(story);
            }
        }
        content.setAuthorList(authorList);
        content.setStoryList(storyList);

        return content;
    }
}
