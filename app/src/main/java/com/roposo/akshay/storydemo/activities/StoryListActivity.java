package com.roposo.akshay.storydemo.activities;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.roposo.akshay.storydemo.adapters.StoryListRecyclerViewAdapter;
import com.roposo.akshay.storydemo.modals.Author;
import com.roposo.akshay.storydemo.modals.Story;
import com.roposo.akshay.storydemo.utils.CommonUtils;
import com.roposo.akshay.storydemo.utils.ContentDeserializer;
import com.roposo.akshay.storydemo.R;
import com.roposo.akshay.storydemo.modals.Content;

import java.io.IOException;

/**
 * Created by AkshaySethi on 09/04/16.
 */
public class StoryListActivity extends AppCompatActivity implements StoryListRecyclerViewAdapter.OnFollowButtonClickListener {

    private StoryListRecyclerViewAdapter storyListRecyclerViewAdapter;
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_story_list);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (toolbar != null) {
            toolbar.setTitle(getTitle());
        }

        recyclerView = (RecyclerView) findViewById(R.id.story_list);
        assert recyclerView != null;

        try {
            setupRecyclerView((RecyclerView) recyclerView);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void setupRecyclerView(@NonNull RecyclerView recyclerView) throws IOException {
        storyListRecyclerViewAdapter = new StoryListRecyclerViewAdapter(this, fetchContentData(CommonUtils.assetJSONFile("test.json", this)).getStoryList(), this);
        recyclerView.setItemViewCacheSize(0);
        recyclerView.setAdapter(storyListRecyclerViewAdapter);
    }


    private Content fetchContentData(String json) {
        // Configure Gson
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(Content.class, new ContentDeserializer());
        Gson gson = gsonBuilder.create();

        // Parse JSON to Java
        Content content = gson.fromJson(json, Content.class);

        content = attachAuthorsToStories(content);

        return content;
    }

    private Content attachAuthorsToStories(Content content) {
        for (int i = 0; i < content.getStoryList().size(); i++) {
            for (int j = 0; j < content.getAuthorList().size(); j++) {
                if (content.getStoryList().get(i).getDb().equals(content.getAuthorList().get(j).getId())) {
                    content.getStoryList().get(i).setAuthor(content.getAuthorList().get(j));
                }
            }
        }
        return content;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            Author author = (Author) data.getSerializableExtra("author");
            Boolean isFollowing = data.getBooleanExtra("isFollowing", false);
            onFollowButtonClick(author, isFollowing);
        }
    }


    @Override
    public void onFollowButtonClick(Author author, boolean isFollowing) {
        int first = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();
        int last = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();


        // This is being done to handle cached views (reducing initial position by 1 and increasing last position by 1)
        first = (first == 0) ? first : first - 1;
        last = (last >= recyclerView.getAdapter().getItemCount() - 1) ? last : last+1;

        // only changing values for visible positions, rest will be taken care of when redrawn
        for (int i = first; i <= last; i++) {

            // to handle null pointer for a non visible child
            View visibleChild = recyclerView.getChildAt(i-first);
            if (visibleChild != null) {

                StoryListRecyclerViewAdapter.ViewHolder holder = ((StoryListRecyclerViewAdapter.ViewHolder) recyclerView.getChildViewHolder(visibleChild));

                if (holder.mStory.getAuthor() == null && author == null) {
                    holder.mStory.getAuthor().setIsFollowing(isFollowing);
                } else if (holder.mStory.getAuthor() != null && author != null && holder.mStory.getAuthor().getUsername().equals(author.getUsername())) {
                    holder.mStory.getAuthor().setIsFollowing(isFollowing);
                }

                // to change values for visible positions
                if (holder.mStory.getAuthor().getIsFollowing()) {
                    holder.btnFollow.setText("Following");
                    holder.btnFollow.setBackgroundResource(R.drawable.btn_following);
                    holder.btnFollow.setTextColor(Color.WHITE);
                } else {
                    holder.btnFollow.setText("Follow");
                    holder.btnFollow.setBackgroundResource(R.drawable.btn_follow);
                    holder.btnFollow.setTextColor(Color.BLACK);
                }
            }
        }
    }
}
