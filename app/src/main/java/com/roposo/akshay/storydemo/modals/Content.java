package com.roposo.akshay.storydemo.modals;

import java.util.ArrayList;

/**
 * Created by AkshaySethi on 09/04/16.
 */
public class Content {

    private ArrayList<Story> storyList;

    private ArrayList<Author> authorList;

    public ArrayList<Story> getStoryList() {
        return storyList;
    }

    public void setStoryList(ArrayList<Story> storyList) {
        this.storyList = storyList;
    }

    public ArrayList<Author> getAuthorList() {
        return authorList;
    }

    public void setAuthorList(ArrayList<Author> authorList) {
        this.authorList = authorList;
    }
}
