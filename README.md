# README #

The repository contains code for assignment shared by Roposo team.
Download demo app v1.1 from [here](https://bitbucket.org/sethi_akshay/roposo-demo/downloads/app-debug-v1.1.apk)

There might be a few additions for better look and feel (better UI/UX). But the app contains pretty much all the functionality given in the shared assignment.

## Revision History ##

### v1.0 ###
* initial demo app

### v1.1 ###
* added changes for removing **notifyitemchanged**
* **fixed flicker issue** while clicking on 'Follow' button